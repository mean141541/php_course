from  db_connect import  *
import datetime

class List_newsClass:
    def __init__(self, id_news=None ,title='name', detail='news', date_post=None, date_grant=None, date_update=None, status='status', id_type_news=2, user_name='user2'):
        self.id = id_news
        self.title_news = title
        self.detail_news = detail
        self.date_post_news = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S.%f")
        self.date_grant_news = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S.%f")
        self.date_update_news = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S.%f")
        self.status_news = status
        self.id_typenews = id_type_news
        self.username = user_name

    def select_Allnews(self):#เลือกข้อมูลข่าวงทั้งหมด
        try:
            sql_select_Allnews= "SELECT * FROM news"
            list_news = getdata(sql_select_Allnews)
            return list_news
        except Exception as e:
            print('errer = ',e)
            return {'msg': 'error'}, 500

    def inset_news(self):#นำข้อมูลข่าวใส่ในฐานข้อมูล
        try:
            sql_insert_news=("INSERT INTO news (title_news, detail_news, date_post_news, date_grant_news ,date_update_news, status_news, id_typenews, username) "
                             "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %s, '%s')"
                             %(self.title_news, self.detail_news, self.date_post_news, self.date_grant_news, self.date_update_news, self.status_news, self.id_typenews, self.username))
            if not (execute(sql_insert_news)):
                return {'msg': 'internal error'}, 500
            else:
                return {'msg': 'success'}, 201
        except Exception as e:
            print ('err = ',e)
            return {'msg':'internal error'}, 500

    def select_news(self):#แสดงข่าวจากชื่อข่าว
        try:
            sql_select_news=("SELECT DISTINCT title_news, detail_news, username FROM news "
                             "where title_news LIKE '%s' OR detail_news LIKE '%s' OR id_typenews = %s"
                             %(self.title_news, self.detail_news, self.id_typenews))

            list_news = getdata(sql_select_news)
            return (list_news)
        except Exception as e:
            print ('errer = ',e)
            return {'msg': 'internal error'}, 500

    def update_news(self):#แก้ไขข้อมูลในข่าว
        try:
            sql_update_news = ("UPDATE news SET title_news= '%s', detail_news= '%s', date_update_news= '%s', status_news= '%s' WHERE id_news='%s'"
                               %(self.title_news, self.detail_news, self.date_update_news, self.status_news, self.id))

            if not (execute(sql_update_news)):
                return {'msg': 'internal error'}, 500
            else:
                return {'msg': 'success'}, 201
        except Exception as e:
            print('errer = ', e)
            return {'msg': 'internal error'}, 500

    def delete_news(self):
        try:
            sql_delete_news = ("DELETE FROM news WHERE id_news='%s'"%self.id)
            if not (execute(sql_delete_news)):
                return {'msg': 'internal error'}, 500
            else:
                return {'msg': 'success'}, 201
        except Exception as e:
            print ('errer = ', e)
            return {'msg': 'internal error'}, 500

#list_class = List_newsClass()
#print (list_class.select_news())

