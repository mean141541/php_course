import mysql.connector

config = {
    "host": 'www.mean-serve.xyz',
    "user": 'administrator',
    "passwd": 'QWer!@34',
    "database": "workshop_news"
}


def getdata(sql):
    TAG = 'DB connect : '
    db_connector = mysql.connector.connect(**config)
    try:
        # print(db_connector.is_connected())
        db_cursor = db_connector.cursor()
        db_cursor.execute(sql)
        result = db_cursor.fetchall()
        mykey = db_cursor.column_names
        db_cursor.close()
        db_connector.close()
        res = result,mykey
        key = []
        val = []
        for i in res[1]:
            key.append(i)
        for x in res[0]:
            temp_val = []
            for y in x:
                temp_val.append(y)
            val.append(temp_val)
        values = []
        for z in val:
            dict_temp = {}
            for a in range(len(key)):
                dict_temp[key[a]] = z[a]
            values.append(dict_temp)
        return values
    except Exception as inst:
        if db_connector.is_connected():
            db_connector.close()
        print(TAG, "dbcon", inst)
        return {"message": "database dose not connect"}, 500


def execute(sql):
    db_connector = mysql.connector.connect(**config)
    try:
        db_cursor = db_connector.cursor()
        db_cursor.execute(sql)
    except Exception as er:
        if db_connector.is_connected():
            db_connector.close()
        print('execute er = ', er)
        return False
    else:
        db_connector.commit()
        db_cursor.close()
        db_connector.close()
        return True
