from db_connect import *
import datetime
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)


class CommentClass:
    def __init__(self, username=None, id_news=None, detail_comment=None,id_comment=None):
        self.username = username
        self.id_comment = id_comment
        self.id_news = id_news
        self.detail_comment = detail_comment
        self.date = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S.%f")

    def keep_comment(self):
        TAG = "keep_comment : "
        try:
            if not self.validate_username():
                return {"msg": "permission denied",
                        'token': create_access_token(identity=self.username)
                        }, 401
            else:
                sql = "INSERT (username,detail_comment,datetime_comment,id_news) VALUES ('%s','%s','%s','%s')" % (self.username,self.detail_comment,self.date,self.id_news)
                if not execute(sql):
                    return {'msg': 'internal error',
                            'token': create_access_token(identity=self.username)
                            }, 500
                else:
                    return {'msg': 'Success',
                            'token': create_access_token(identity=self.username)
                            }, 201
        except Exception as er:
            print(TAG,'er = ', er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500


    def validate_username(self):
        TAG = "validate_username : "
        try:
            sql = "SELECT * FROM user u,user_data ud,type t WHERE u.username = '%s' AND u.username = ud.username AND t.id_type = u.id_type "% self.username
            result = getdata(sql)
            if not result:
                return False
            else:
                return True
        except Exception as er:
            print(TAG,'er = ', er)
            return False

    def get_all_comment(self):
        TAG = "get_all_comment : "
        try:
            if not self.validate_username():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            sql = "SELECT cm.id_comment,cm.datetime_comment,cm.detail_comment,cm.username,cm.id_comment,cm.id_news,n.title_news,n.detail_news,n.date_post_news,n.date_grant_news,n.date_update_news,n.status_news,tn.name_typenews FROM comment cm,news n,type_news tn WHERE tn.id_typenews = n.id_typenews AND n.id_news = cm.id_news"
            data = getdata(sql)
            return {'msg':'success',
                    'data': data,
                    'token': create_access_token(identity=self.username)
                    }, 200
        except Exception as er:
            print(TAG,'er = ', er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500

    def get_some_comment(self):
        TAG = "get_some_comment : "
        try:
            if not self.validate_username():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            else:
                sql = "SELECT cm.id_comment,cm.datetime_comment,cm.detail_comment,cm.username,cm.id_comment,cm.id_news,n.title_news,n.detail_news,n.date_post_news,n.date_grant_news,n.date_update_news,n.status_news,tn.name_typenews FROM comment cm,news n,type_news tn WHERE tn.id_typenews = n.id_typenews AND n.id_news = cm.id_news AND id_comment = %s" % self.id_comment
                result = getdata(sql)
                return {'msg': 'success',
                        'data': result,
                        'token': create_access_token(identity=self.username)
                        }, 200
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500

    def delete_comment(self):
        TAG = "delete_comment : "
        try:
            if not self.validate_username():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            sql = "DELETE FROM comment WHERE id_comment = %s"% self.id_comment
            if not execute(sql):
                return {'msg':'internal error',
                        'token': create_access_token(identity=self.username)
                        }, 500
            else:
                return {'msg':'success',
                        'token': create_access_token(identity=self.username)
                        }, 200
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500

