from flask import Flask, request, jsonify
from login_dir.LoginClass import *
from login_dir.RegisterClass import *
from comment_dir.CommentClass import *
from login_dir.UsermanagementClass import *
from news_dir.news_list import *
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)


app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'php_workshop_course'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 600000
jwt = JWTManager(app)


@app.route('/login', methods=['POST'])
def login_function():
    try:
        req = request.json
        username = req['username']
        password = req['password']
    except Exception as er:
        print('does not exist username')
        return {'msg': 'error'}, 400
    else:
        request_ready = {"username": username,
                         "password": password,
                         }
        login = LoginClass(**request_ready)
        return jsonify(login.login())


@app.route('/password_reset', methods=['POST'])
def password_reset_function():
    try:
        req = request.json
        username = req['username']
        email = req['email']
    except Exception as er:
        print('does not exist username')
        return {'msg': 'error'}, 400
    else:
        request_ready = {"username": username,
                         "email": email
                         }
        user_management = UsermanagementClass(**request_ready)
        return jsonify(user_management.password_reset())


@app.route('/delete_user', methods=['POST'])
@jwt_required
def delete_user_function():
    try:
        req = request.json
        username = get_jwt_identity()
        user_for_delete = req['username_for_delete']
    except Exception as er:
        print('does not exist username')
        return {'msg': 'error'}, 400
    else:
        if username == user_for_delete:
            return {'msg': 'can not delete yourself'}, 412
        request_ready = {"username": username,
                         "user_for_delete": user_for_delete
                         }
        user_management = UsermanagementClass(**request_ready)
        return jsonify(user_management.delete_user())


@app.route('/edit_information', methods=['POST'])
@jwt_required
def edit_information_function():
    try:
        req = request.json
        username = get_jwt_identity()
        try:
            password = req['password']
        except:
            password = None
        name = req['name']
        surname = req['surname']
        email = req['email']
        phonenumber = req['phonemunber']
        rank = req['rank']
    except Exception as er:
        print('does not exist username')
        return {'msg': 'error'}, 400
    else:
        request_ready = {"username": username,
                         "password":password,
                         "phonenumber": phonenumber,
                         "name": name,
                         "surname": surname,
                         "email": email,
                         "rank": rank
                         }
        user_management = UsermanagementClass(**request_ready)
        return jsonify(user_management.edit_data())


@app.route('/register', methods=['POST'])
def register_function():
    try:
        print(request.json)
        req = request.json
        username = req['username']
        password = req['password']
        name = req['name']
        surname = req['surname']
        email = req['email']
        phonenumber = req['phonemunber']
        rank = req['rank']
    except Exception as er:
        print(er)
        return {'msg': 'error'}, 400
    else:
        if (len(username) > 15 or username is None) or \
                (len(password) > 15 or password is None) or \
                (len(name) > 50 or name is None) or \
                (len(surname) > 50 or surname is None) or \
                (len(email) > 50 or email is None) or \
                (len(phonenumber) > 10 or phonenumber is None) or \
                (len(rank) > 20 or rank is None):
            return {'msg': 'values is over length'}, 400
        try:
            int(phonenumber)
        except:
            return {'msg': 'phone number invalid'}, 400
        request_ready = {
            "username": username,
            "password": password,
            "name": name,
            "surname": surname,
            "email": email,
            "phonenumber": phonenumber,
            "rank": rank
        }
        register = RegisterClass(**request_ready)
        return jsonify(register.register())


@app.route('/comment', methods=['POST'])
@jwt_required
def comment_function():
    try:
        req = request.json
        detail_comment = req['detail_comment']
        username = get_jwt_identity()
        id_news = req['id_news']
    except Exception as err:
        print(err)
        return {'msg': 'error'}, 400
    else:
        req_ready = {
            "detail_comment": detail_comment,
            "username": username,
            "id_news": id_news
        }
        comment = CommentClass(**req_ready)
        return jsonify(comment.keep_comment())


@app.route("/news", methods=['GET'])
@jwt_required
def get_news():
    try:
        news = List_newsClass()
        data = news.select_Allnews()
        return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/news_insert", methods=['POST'])
@jwt_required
def news_insert():
    try:
        req = request.json
        try:
            title = req['title']
            detail = req['detail']
            status = req['status']
            id_type_news = req['id_type_news']
            user_name = req['user_name']
        except Exception as er:
            print(er)
            return {'msg': 'error'}, 400
        else:
            req_ready = {
                "title": title,
                "detail": detail,
                "status": status,
                "id_type_news": id_type_news,
                "user_name": user_name
            }
            news = List_newsClass(**req_ready)
            data = news.inset_news()
            return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/news_some", methods=['POST'])
@jwt_required
def news_some():
    try:
        req = request.json
        try:
            title = req['title']
            detail = req['detail']
            id_type_news = req['id_type_news']
        except Exception as er:
            print(er)
            return {'msg': 'error'}, 400
        else:
            req_ready = {
                "title": title,
                "detail": detail,
                "id_type_news": id_type_news
            }
            news = List_newsClass(**req_ready)
            data = news.select_news()
            return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/news_update", methods=['POST'])
@jwt_required
def news_update():
    try:
        req = request.json
        try:
            title = req['title']
            id_news = req['id_news']
            detail = req['detail']
            status = req['status']
            id_type_news = req['id_type_news']
        except Exception as er:
            print(er)
            return {'msg': 'error'}, 400
        else:
            req_ready = {
                "title": title,
                "id_news": id_news,
                "detail": detail,
                "status": status,
                "id_type_news": id_type_news
            }
            news = List_newsClass(**req_ready)
            data = news.update_news()
            return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/news_delete", methods=['POST'])
@jwt_required
def news_delete():
    try:
        req = request.json
        try:
            id_news = req['id_news']
        except Exception as er:
            print(er)
            return {'msg': 'error'}, 400
        else:
            req_ready = {
                "id_news": id_news

            }
            news = List_newsClass(**req_ready)
            data = news.delete_news()
            return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500



@app.route("/all_user_approved_yet", methods=['GET'])
@jwt_required
def all_user_approved_yet():
    try:
        username = get_jwt_identity()
        request_ready = {
            "username": username
        }
        user = UsermanagementClass(**request_ready)
        data = user.get_all_non_approve()
        return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/approve", methods=['POST'])
@jwt_required
def approve():
    try:
        username = get_jwt_identity()
        req = request.json
        try:
            user_for_approve = req['user_for_approve']
        except Exception as er:
            print('er = ', er)
            return {'msg': 'error'},400
        request_ready = {
            "username": username,
            "user_for_approve": user_for_approve
        }
        user = UsermanagementClass(**request_ready)
        data = user.approve_user()
        return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/comment_all", methods=['GET'])
@jwt_required
def comment_all():
    try:
        username = get_jwt_identity()
        req_ready = {
            'username': username
        }
        user = UsermanagementClass(**req_ready)
        data = user.approve_user()
        return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/comment_delete", methods=['POST'])
@jwt_required
def comment_delete():
    try:
        username = get_jwt_identity()
        req = request.json
        try:
            id_comment = req['id_comment']
        except Exception as er:
            print(er)
            return {'msg':'error'},400
        else:
            req_ready = {
                'username': username,
                "id_comment": id_comment
            }
            comment = CommentClass(**req_ready)
            data = comment.delete_comment()
            return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


@app.route("/comment_some", methods=['POST'])
@jwt_required
def comment_some():
    try:
        username = get_jwt_identity()
        req = request.json
        try:
            id_comment = req['id_comment']
        except Exception as er:
            print(er)
            return {'msg':'error'},400
        else:
            req_ready = {
                'username': username,
                "id_comment": id_comment
            }
            comment = CommentClass(**req_ready)
            data = comment.get_some_comment()
            return jsonify(data)
    except Exception as er:
        print(er)
        return {'msg':'error'}, 500


app.run(host='0.0.0.0', port=5000, debug=True)
