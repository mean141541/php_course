from db_connect import *
import random
import string
import bcrypt
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)


class UsermanagementClass:

    def __init__(self, username=None, name=None, surname=None, email=None, phonenumber=None, rank=None,user_for_delete=None,password=None,user_for_approve=None):
        self.username = username
        self.name = name
        self.surname = surname
        self.email = email
        self.phonenumber = phonenumber
        self.rank = rank
        self.user_for_delete = user_for_delete
        self.password = password
        self.user_for_approve = user_for_approve

    def password_reset(self):
        TAG = "password_reset : "
        try:
            if not self.validate_username_exist():
                return {'msg': 'permission denied',
                        'token':create_access_token(identity=self.username)
                        }, 401
            else:
                sql_get_email = "SELECT email FROM user WHERE username = '%s' AND email = '%s'" % (self.username,self.email)
                result_email = getdata(sql_get_email)
                if not result_email:
                    return {'msg': 'permission denied',
                            'token':create_access_token(identity=self.username)
                            }, 401
                else:
                    email = result_email[0]['email']
                    new_password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
                    hashed = bcrypt.hashpw(new_password.encode('utf8'), bcrypt.gensalt(16))
                    hashed = hashed.decode('utf8')
                    sql_update_password = "UPDATE user SET password = '%s' WHERE username = '%s'" % (
                    hashed, self.username)
                    if not execute(sql_update_password):
                        return {'msg': 'internal error',
                                'token': create_access_token(identity=self.username)
                                }, 500
                    else:
                        return {'msg': 'success',
                                'email': email,
                                'token': create_access_token(identity=self.username),
                                'new_password': new_password}, 200
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500

    def edit_data(self):
        TAG = "edit_data : "
        try:
            if not self.validate_username_exist():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            if self.password is None or self.password == '':
                sql = "UPDATE user_data SET name = '%s',surname = '%s',email = '%s', phonemunber = '%s',rank = '%s' WHERE username = '%s'" % (
                self.name, self.surname, self.email, self.phonenumber, self.rank, self.username)
                if not execute(sql):
                    return {'msg': 'internal error',
                            'token': create_access_token(identity=self.username)
                            }, 500
                else:
                    return {'msg': 'success',
                            'token': create_access_token(identity=self.username)
                            }, 200
            else:
                hashed = bcrypt.hashpw(self.password.encode('utf8'), bcrypt.gensalt(16))
                hashed = hashed.decode('utf8')
                sql = "UPDATE user_data SET password = '%s', name = '%s',surname = '%s',email = '%s', phonemunber = '%s',rank = '%s' WHERE username = '%s'" % (
                    hashed,self.name, self.surname, self.email, self.phonenumber, self.rank, self.username)
                if not execute(sql):
                    return {'msg': 'internal error',
                            'token': create_access_token(identity=self.username)
                            }, 500
                else:
                    return {'msg': 'success',
                            'token': create_access_token(identity=self.username)
                            }, 200
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500


    def delete_user(self):
        TAG = "delete_user : "
        try:
            if not self.validate_username_exist():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            if not self.validate_permission():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            sql = "DELETE FROM user WHERE username = '%s'" % self.user_for_delete
            if not execute(sql):
                return {'msg': 'internal error',
                        'token': create_access_token(identity=self.username)
                        }, 500
            else:
                return {'msg': 'success',
                        'token': create_access_token(identity=self.username)
                        }, 200
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500

    def get_all_non_approve(self):
        TAG = "get_all_non_approve : "
        try:
            if not self.validate_username_exist():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            if not self.validate_permission():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            sql = "SELECT ud.name,ud.surname,ud.email,u.username,ud.phonemunber,ud.rank FROM user u,user_data ud WHERE u.username = ud.username AND u.validate_user = 0 "
            result = getdata(sql)
            return {'msg': 'success',
                    'data': result,
                    'token': create_access_token(identity=self.username)
                    }, 500
        except Exception as er:
            print(TAG, "er = ", er)
            return {'msg': 'internal error',
                    'token': create_access_token(identity=self.username)
                    }, 500

    def approve_user(self):
        TAG = "approve_user : "
        try:
            if not self.validate_username_exist():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            if not self.validate_permission():
                return {'msg': 'permission denied',
                        'token': create_access_token(identity=self.username)
                        }, 401
            sql = "UPDATE user SET validate_user = 1 WHERE username = '%s'"%self.user_for_approve
            if not execute(sql):
                return {'msg': 'error',
                        'token': create_access_token(identity=self.username)
                        }, 500
            else:
                return {'msg': 'success',
                        'token': create_access_token(identity=self.username)
                        }, 200
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg':'error',
                    'token': create_access_token(identity=self.username)
                    },500

    def validate_permission(self):
        TAG = "validate_permission : "
        try:
            sql = "SELECT t.name_type AS name_type FROM user u,type t WHERE t.id_type = u.id_type AND u.username = '%s'" % self.username
            result = getdata(sql)
            if not result:
                return False
            else:
                if result[0]['name_type'] == 'admin':
                    return True
                else:
                    return False
        except Exception as er:
            print(TAG, 'er = ', er)
            return False

    def validate_username_exist(self):
        TAG = "validate_username_exist : "
        try:
            sql = "SELECT * FROM user WHERE username = '%s'" % self.username
            result = getdata(sql)
            if not result:
                return False
            else:
                return True
        except Exception as er:
            print(TAG, 'er = ', er)
            return False

