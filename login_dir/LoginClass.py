from db_connect import *
import bcrypt
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)


class LoginClass:
    def __init__(self,username=None, password=None):
        self.username = username
        self.password = password

    def login(self):
        TAG = "login : "
        try:
            password = self.password.encode('utf8')
            sql_get_password = "SELECT password FROM user WHERE username = '%s'"% self.username
            password_data = getdata(sql_get_password)
            if not password_data:
                return {'msg': 'login fail'}, 401
            hashed = password_data[0]['password']
            hashed = hashed.encode('utf8')
            if bcrypt.checkpw(password, hashed):
                sql = "SELECT t.name_type,ud.name,ud.surname,ud.email,ud.phonemunber,ud.rank FROM user u,type t,user_data ud WHERE ud.username = u.username AND u.username = '%s' AND t.id_type = u.id_type AND u.validate_user = 1"% self.username
                data_result = getdata(sql)
                if not data_result:
                    return {'msg': 'login fail'}, 401
                else:
                    access_token = create_access_token(identity=self.username)
                    return {'msg': 'login succerss',
                            'permission': data_result[0]['name_type'],
                            'name': data_result[0]['name'],
                            'surname': data_result[0]['surname'],
                            'email': data_result[0]['email'],
                            'phonemunber': data_result[0]['phonemunber'],
                            'rank': data_result[0]['rank'],
                            'token': access_token
                            }, 200
            else:
                return {'msg': 'login fail'}, 401
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg': 'internal error'}, 500
