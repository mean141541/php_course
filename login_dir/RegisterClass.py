from db_connect import *
import bcrypt
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)


class RegisterClass:
    def __init__(self,username=None,password=None,name=None,surname=None,email=None,phonenumber=None,rank=None):
        self.username = username
        self.password = password
        self.permission = 'member'
        self.name = name
        self.surname = surname
        self.email = email
        self.phone = phonenumber
        self.rank = rank

    def register(self):
        TAG = "register : "
        try:
            if self.duplicate_check():
                hashed = bcrypt.hashpw(self.password.encode('utf8'), bcrypt.gensalt(16))
                hashed = hashed.decode('utf8')
                sql = "INSERT user (username,password,id_type,validate_user) VALUES ('%s','%s',(SELECT id_type FROM type WHERE name_type = '%s' LIMIT 1 ),0)"%(self.username, hashed,self.permission)
                print(sql)
                sql_2 = "INSERT user_data (name,surname,email,phonemunber,rank,username) VALUES ('%s','%s','%s','%s','%s',(SELECT username FROM user WHERE username = '%s' LIMIT 1))" %(self.name,self.surname,self.email,self.phone,self.rank,self.username)

                try:
                    if not execute(sql):
                        return {'msg': 'internal error'}, 500
                    if not execute(sql_2):
                        return {'msg': 'internal error'}, 500
                    access_token = create_access_token(identity=self.username)
                    return {'msg': 'success',
                            'token': access_token}, 201
                except Exception as er:
                    print(er)
                    return {'msg':'internal error'}, 500
            else:
                return {'msg': 'duplicate username'}, 412
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg':'internal error'}, 500
        pass

    def duplicate_check(self):
        TAG = "duplicate_check: "
        try:
            sql = "SELECT * FROM user WHERE username = '%s'" % self.username
            result = getdata(sql)
            if not result:
                return True
            else:
                return False
        except Exception as er:
            print(TAG, 'er = ', er)
            return {'msg': 'internal error'}, 500